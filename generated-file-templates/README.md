# {{fancyBrickName}} Canopsis Brick

## Index

- [Description](#description)
- [Content](#content)
- [Installation](#installation)
- [Usage](#usage)
- [Continuous-integration](#continuous-integration)
- [Code-notes](#code-notes)
- [Additional-info](#additional-info)

## Description

{{description}}

## Content

{{listing}}

## Screenshots

{{screenshots}}

## Installation

You need to clone the git repository and copy directory to Canopsis path

    $ su - canopsis 
    $ cd var/www
    $ ./bin/brickmanager install {{name}}

Then, you need to enable the brick

    $ ./bin/brickmanager enable {{name}}

You can see enabled bricks

    $ su - canopsis
    $ cd var/www
    $ ./bin/brickmanager list
    [u'core', u'uibase', u'monitoring', ..., **u'{{name}}'**]

## Usage

See [Howto](https://git.canopsis.net/canopsis-ui-bricks/{{name}}/blob/master/doc/index.rst)

## Continuous-Integration

### Tests

{{testOutput}}

### Lint

Tested on commit : {{commit}}.

| Target | Status | Log |
| ------ | ------ | --- |
| Lint   | {{lintStatus}} | {{lintLog}} |


## Code-Notes

### TODOS

{{todos}}

### FIXMES

{{fixmes}}

## Additional-info

Minified version : {{minSrcFilesCount}} files (size: {{minSrcFilesSize}})
Development version : {{devSrcFilesCount}} files (size: {{devSrcFilesSize}})
