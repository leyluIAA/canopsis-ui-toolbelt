.. _{{docBrickName}}:

=====================================================================
{{fancyBrickName}} Canopsis Brick
=====================================================================

{{description}}


.. toctree::
   :maxdepth: 2
   :titlesonly:

   README
   FRs
   TRs
   EDs