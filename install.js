var chalk = require('chalk');
var inquirer = require("inquirer");
var fs = require('fs');

console.log(chalk.green.underline('Canopsis-ui-toolbelt installed!'));

function fileExists(filePath) {
    try {
        return fs.statSync(filePath).isFile();
    } catch (err) {
        return false;
    }
}

//As npm rename gitignore files to npmignore, turn back manually the original name
fs.rename('generated-file-templates/.npmignore', 'generated-file-templates/.gitignore');

if(!fileExists('../../manifest.json')) {
  inquirer.prompt([{
    type: 'confirm',
    name: 'initializeProject',
    message: 'This brick does not seems to have a manifest.json file. Would you like to generate a manifest from package.json informations? (Some files might need to be modified)'
  }], function( answers ) {
    console.log('answer', answers);
    if(answers.initializeProject) {
      console.log('Initializing project');
      var npmManifest = require('../../package.json');
      console.log(npmManifest);
      npmManifest.scripts.compile = 'rm -Rf tmp/build && broccoli build tmp/build && cp tmp/build . -RT';

      fs.writeFile('../../package.json', JSON.stringify(npmManifest, null, 2), function (err) {
        if (err) return console.log(err)
        console.log('- package.json written');
      });

      fs.writeFile('../../Brocfile.js', 'var CanopsisUiToolbelt = require(\'canopsis-ui-toolbelt\');\nmodule.exports = CanopsisUiToolbelt.init();', function (err) {
        if (err) return console.log(err)
        console.log('- Brocfile.js written');
      });
      
      delete npmManifest.devDependencies;
      delete npmManifest.dependencies;
      delete npmManifest.scripts;
      delete npmManifest.main;
      delete npmManifest.fancyBrickName;
      delete npmManifest.screenshots;
      delete npmManifest['pre-commit'];

      fs.writeFile('../../manifest.json', JSON.stringify(npmManifest, null, 2), function (err) {
        if (err) return console.log(err)
        console.log('- manifest.json written');
      });
    }
  });
}