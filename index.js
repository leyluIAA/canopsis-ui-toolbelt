/*
 * Copyright (c) 2015 "Capensis" [http://www.capensis.com]
 *
 * This file is part of Canopsis.
 *
 * Canopsis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Canopsis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Canopsis. If not, see <http://www.gnu.org/licenses/>.
 */

var generate = require('broccoli-auto-generated');
var manifestJson = require('./../../manifest.json');
var fs = require('fs');
var mkdirp = require('mkdirp');
var recursiveReadSync = require('recursive-readdir-sync');
var syncExec = require('sync-exec');
var git = require('git-rev-sync');
var find = require('find');

var docstrings;

try {
    docstrings = require('./../../docstrings.json');
} catch (e) {
    docstrings = {};
}

function stringStartsWith(string, prefix) {
    return string.slice(0, prefix.length) == prefix;
}

function stringEndsWith (string, suffix) {
    return suffix == '' || string.slice(-suffix.length) == suffix;
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function grepContent(needle, rootDirectory) {
    var lintResults = syncExec('grep -R "'+ needle +'" src');
    var greppedContent = lintResults.stdout.split('\n');
    var result = '| File   | Note   |\n|--------|--------|\n';

    if(greppedContent.length === 1) return '';

    for (var i = 0; i < greppedContent.length; i++) {
        var lineChunk = greppedContent[i].split(needle)[1];
        var concernedFile = greppedContent[i].split(':')[0];
        if(lineChunk) {
            result += '| ' + concernedFile + ' |' + lineChunk + ' |\n';
        }
    }
    return result;
}

function generateInitJsPathsMin(brickVariables) {
    var srcFiles = brickVariables.minSrcFiles || [];
    var requireJsPaths = '';
    var requirements = ['\'text!canopsis/'+ brickVariables.name +'/dist/templates.min.html\''];

    for(var i = 0; i < srcFiles.length; i++) {
        var fileUrl = srcFiles[i];
        if(stringStartsWith(fileUrl, 'dist/templates') && stringEndsWith(fileUrl, '.hbs')) {
            templateName = fileUrl.slice(15).slice(0, -4);
            requireJsPaths += '        \'' + templateName + '\': \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -4) + '\',\n';
            requirements.push('\n    \'ehbs!' + templateName + '\'');
        } else if(stringEndsWith(fileUrl, '.css')) {
            requirements.push('\n    \'link!canopsis/' + brickVariables.name + '/' + fileUrl + '\'');
        } else if(!stringStartsWith(fileUrl, 'src/') && !stringStartsWith(fileUrl, 'dist/')){
            requirements.push('\n    \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -3) + '\'');
        }
    }
    requirements.push('\n    \'canopsis/' + brickVariables.name + '/dist/brick.min\'');

    requirements += '\n';

    brickVariables.minRequireJsPaths = requireJsPaths;
    brickVariables.minRequirements = requirements;
}

function generateInitJsPathsDev(brickVariables) {
    var srcFiles = brickVariables.devSrcFiles || [];
    var requireJsPaths = '';
    var requirements = [];

    for(var i = 0; i < srcFiles.length; i++) {
        var fileUrl = srcFiles[i];
        if(stringStartsWith(fileUrl, 'src/components') && stringEndsWith(fileUrl, '.hbs')) {
            componentName = fileUrl.slice(15).split('/')[0];
            requireJsPaths += '        \'components/component-' + componentName + '\': \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -4) + '\',\n';
            requirements.push('\n    \'ehbs!components/component-' + componentName + '\'');
        } else if(stringEndsWith(fileUrl, '.hbs')) {
            var splittedUrl = fileUrl.split('/');
            templateName = splittedUrl[splittedUrl.length - 1].slice(0, -4);
            requireJsPaths += '        \'' + templateName + '\': \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -4) + '\',\n';
            requirements.push('\n    \'ehbs!' + templateName + '\'');
        } else if(stringEndsWith(fileUrl, '.css')) {
            requirements.push('\n    \'link!canopsis/' + brickVariables.name + '/' + fileUrl + '\'');
        } else {
            requirements.push('\n    \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -3) + '\'');
        }
    }
    requirements += '\n';

    brickVariables.devRequireJsPaths = requireJsPaths;
    brickVariables.devRequirements = requirements;

    var testRequirements = [];
    var testsFiles = brickVariables.testsFiles || [];

    for(var i = 0; i < testsFiles.length; i++) {
        var fileUrl = testsFiles[i];
        testRequirements.push('\n    \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -3) + '\'');
    }
    testRequirements += '\n';

    brickVariables.testRequirements = testRequirements;
}

function dirExistsSync (d) {
  try { fs.statSync(d); return true }
  catch (er) { return false }
}

/**
 * @function generateBrickListing
 * @description create a brick listing from the docstring structure
 * @param brickVariables {dict} the main template context dict
 * @param docstrings {dict} the docstring json tree
 */
function generateBrickListing(brickVariables, docstrings) {
    brickVariables.listing = '';

    Object.keys(docstrings).forEach(function(key) {
        brickVariables.listing += '### ' + key + '\n\n';

        for (var i = 0; i < docstrings[key].length; i++) {
            brickVariables.listing += ' - ' + docstrings[key][i].name + '\n' ;
        }

        brickVariables.listing += '\n' ;
    });
}

var CanopsisUiToolbelt = {
    init: function () {
        brickVariables = manifestJson;

        brickVariables.repository_type = manifestJson.repository.type;
        brickVariables.repository_url = manifestJson.repository.url;

        if(brickVariables.name.substring(0, 6) === 'brick-') {
            brickVariables.fancyBrickName = brickVariables.name.substring(6);
        } else {
            brickVariables.fancyBrickName = brickVariables.name;
        }
        brickVariables.docBrickName = brickVariables.fancyBrickName;
        brickVariables.fancyBrickName = brickVariables.fancyBrickName.charAt(0).toUpperCase() + brickVariables.fancyBrickName.slice(1);
        brickVariables.screenshots = "";

        var path = 'doc/preview';
        mkdirp.sync(path);
        mkdirp.sync('dist');
        mkdirp.sync('schemas');

        var files = fs.readdirSync(path);

        for (var i = 0, l = files.length; i < l; i++) {
            brickVariables.screenshots += "![View" + i +"](https://git.canopsis.net/canopsis-ui-bricks/"+ brickVariables.name +"/raw/master/doc/preview/"+ files[i] + ")";
        }

        brickVariables.schemasRequirements = recursiveReadSync('schemas');4
        var minifiedArray = [];
        for (var i = 0; i < brickVariables.schemasRequirements.length; i++) {
            var currentSchema = JSON.parse(fs.readFileSync(brickVariables.schemasRequirements[i], 'utf8'));
            var schemaId = brickVariables.schemasRequirements[i].split('/').pop().split('.json')[0];
            var schemaDocument = {
                id: schemaId,
                _id: schemaId,
                crecord_name: schemaId.split('.').pop(),
                schema: currentSchema
            };
            brickVariables.schemasRequirements[i] = '"text!canopsis/' + brickVariables.name + '/' + brickVariables.schemasRequirements[i] + '"';
            minifiedArray.push(schemaDocument);
        }
        brickVariables.schemasMinified = JSON.stringify(minifiedArray);

        brickVariables.devSrcFiles = recursiveReadSync('src');
        brickVariables.devSrcFilesCount = brickVariables.devSrcFiles.length;
        brickVariables.devSrcFilesSize = syncExec('du -sh src').stdout.split(/\s+/)[0];

        brickVariables.minSrcFiles = recursiveReadSync('dist');
        brickVariables.minSrcFilesCount = brickVariables.minSrcFiles.length;
        brickVariables.minSrcFilesSize = syncExec('du -sh dist').stdout.split(/\s+/)[0];

        if (dirExistsSync('requirejs-modules')) {
            //TODO optimize this
            brickVariables.devSrcFiles = brickVariables.devSrcFiles.concat(recursiveReadSync('requirejs-modules'));
            brickVariables.minSrcFiles = brickVariables.minSrcFiles.concat(recursiveReadSync('requirejs-modules'));
        }

        brickVariables.devInitJsPaths = generateInitJsPathsDev(brickVariables);
        brickVariables.minInitJsPaths = generateInitJsPathsMin(brickVariables);

        if(brickVariables.envMode === 'production') {
            brickVariables.srcFiles = brickVariables.minSrcFiles;
            brickVariables.requireJsPaths = brickVariables.minRequireJsPaths;
            brickVariables.requirements = brickVariables.minRequirements;
            brickVariables.initArguments = ['templates'];
            brickVariables.initBody = "templates = $(templates).filter('script');\nfor (var i = 0, l = templates.length; i < l; i++) {\nvar tpl = $(templates[i]);\nEmber.TEMPLATES[tpl.attr('data-template-name')] = Ember.Handlebars.compile(tpl.text());\n};";
        } else {
            brickVariables.srcFiles = brickVariables.devSrcFiles;
            brickVariables.requireJsPaths = brickVariables.devRequireJsPaths;
            brickVariables.requirements = brickVariables.devRequirements;
            brickVariables.initArguments = [];
            brickVariables.initBody = "";
        }
        brickVariables.bowerDependencies = brickVariables.bowerDependencies ? JSON.stringify(brickVariables.bowerDependencies) : '{}';

        if(dirExistsSync('tests')) {
            brickVariables.testsFiles = recursiveReadSync('tests') || [];

            var testRequirements = [];

            for(var i = 0; i < brickVariables.testsFiles.length; i++) {
                var fileUrl = brickVariables.testsFiles[i];
                testRequirements.push('\n    \'canopsis/' + brickVariables.name + '/' + fileUrl.slice(0, -3) + '\'');
            }
            testRequirements += '\n';

            brickVariables.testRequirements = testRequirements;
        }

        if(process.argv[4] === 'full-build') { //check if a "full-build" keyword is passed as argument to broccoli
            var testCommand = 'cd ../../../ && testem ci . -t "en/static/canopsis/index.test.html?module='+ brickVariables.name +'" -f testem-toolbelt.js';
            brickVariables.testOutput = syncExec(testCommand).stdout;
        } else {
            brickVariables.testOutput = 'The last build was not a full build. Please use the "full-compile" npm script to make test results show up here.';
        }

        var lintResults = syncExec('eslint src');
        brickVariables.date = new Date().toISOString().replace('T', ' ').replace('Z', '');
        try{
            brickVariables.commit = git.short();
        } catch (e) {
            brickVariables.commit = '[ERROR : The brick is not in a dedicated git repository]';
        }
        brickVariables.lintStatus = lintResults.status ? ':negative_squared_cross_mark: ERROR': ':ok: OK';
        brickVariables.lintLog = replaceAll(lintResults.stdout, '\n', '<br>');

        brickVariables.todos = grepContent('//TODO', 'src');
        brickVariables.fixmes = grepContent('//FIXME', 'src');

        brickVariables.docstrings = docstrings;
        generateBrickListing(brickVariables, docstrings);

        //process "generator" folder files with values, license content and default values
        var compiledTemplateTrees = generate('node_modules/canopsis-ui-toolbelt/generated-file-templates', {
          values: brickVariables,
          files: ['*', '.eslintignore', '.eslintrc', '.jsdoc.conf.json', '.bowerrc', '.gitignore']
        });

        return compiledTemplateTrees;
    }
};


module.exports = CanopsisUiToolbelt;
