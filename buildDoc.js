var recursiveReadSync = require('recursive-readdir-sync');
var mkdirp = require('mkdirp');
var fs = require('fs');
var syncExec = require('sync-exec');

//Clean last builds and tmp files
syncExec('rm -r doc/jsdoc && rm -r tmp/jsdocsrc');

mkdirp.sync('./tmp/jsdocsrc/editors');
mkdirp.sync('./tmp/jsdocsrc/renderers');
mkdirp.sync('./tmp/jsdocsrc/schemas');

/*
 * Add to index.rst later
 * Api documentation<../../_static/bricks-documentation/{{name}}/jsdoc/index.html#http://>
 */
function generateJsFiles(elements, tagName) {
    for (var i = 0; i < elements.length; i++) {
        var elementName = elements[i].split('/').pop().split('.')[0];
        var prefix = tagName + '-';
        elementName = elementName.substr(prefix.length);
        var fileContent = fs.readFileSync(elements[i], "utf8");
        var docString = fileContent.match(/\{\{\!\*([^\}]{2}|\n)*\}\}/);
        if(docString && docString.length) {
            docString = docString[0];
            docString = docString.replace('{{!*', '/**');
            docString = docString.replace('}}', '*/');
        } else {
            docString = '/**\n * @ ' + tagName + ' ' + elementName + '\n */';
        }

        fs.writeFileSync('./tmp/jsdocsrc/'+ tagName +'s/' + elementName + '.js', docString, 'utf8');
    }
}


try {
    var editors = recursiveReadSync('src/editors');
    generateJsFiles(editors, 'editor');
} catch (e) {}

try {
    var renderers = recursiveReadSync('src/renderers');
    generateJsFiles(renderers, 'renderer');
} catch (e) {}

try {
    var schemas = recursiveReadSync('schemas') || [];
    for (var i = 0; i < schemas.length; i++) {
        var elementName = schemas[i].split('/').pop().split('.').slice(-2, -1).join('.');
        var fileContent = fs.readFileSync(schemas[i], "utf8");
        fileContent = JSON.parse(fileContent);
        var docString = '/** \n * @schema '+ elementName +'\n';
        if(fileContent.description) {
            docString += ' * @description ' + fileContent.description + '\n';
        }
        docString += ' */\n var schema = Ember.Object.create({\n';

        for(var propName in fileContent.properties) {
            var propContent = fileContent.properties[propName];
            docString += '\t/** \n\t * @property ' + propName + '\n';
            if(propContent.type) {
                docString += '\t * @type ' + propContent.type + ' \n';
            }
            if(propContent.role) {
                docString += '\t * @role ' + propContent.role + ' \n';
            }
            if(propContent.description) {
                docString += '\t * @description ' + propContent.description + ' \n';
            }
            docString += '*/\n'+ propName +': undefined,\n';
        }
        docString += '});\n';
        fs.writeFileSync('./tmp/jsdocsrc/schemas/' + elementName + '.js', docString, 'utf8');
    }
} catch (e) {}

