/*
 * Copyright (c) 2015 "Capensis" [http://www.capensis.com]
 *
 * This file is part of Canopsis.
 *
 * Canopsis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Canopsis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Canopsis. If not, see <http://www.gnu.org/licenses/>.
 */

var fs = require('fs');
var find = require('find');
var CleanCSS = require('clean-css');
var uglifyJS = require('uglify-js');
var mkdirp = require('mkdirp');
var filecopy = require('filecopy');
var syncExec = require('sync-exec');

//Clean last builds and tmp files
syncExec('rm -r dist');

// Create dist directory for minified sources
mkdirp('dist/templates/components', function (err) {
  if (err) console.log(err);
});

// declarations and options
var style = '';
var options = {
  target: 'dist/brick.min.css'
};
var jsTab = [];

/**
 * For each file in src directory,
 * do something appropriate for minification
 */
var templateFileContent = '';

find.eachfile('src', function (file) {
  var extension = file.split('.')[1];

  // put all js files in a tab
  if (extension === 'js') {
    jsTab.push(file);
  }
  // copy hbs files in dist directory
  if (extension === 'hbs') {
    var sourceFile = file.split('/').pop();
    var destName = '';
    var destFile = '';
    if (sourceFile === 'template.hbs') {
      destName = 'components/component-' + file.split('/').reverse()[1];
      destFile = 'dist/templates/components/component-' + file.split('/').reverse()[1] + '.hbs';
    }
    else {
      destName = sourceFile.split('.hbs')[0];
      destFile = 'dist/templates/' + sourceFile;
    }
    templateFileContent += '<script type="text/x-handlebars" data-template-name="'+ destName +'">' + fs.readFileSync(file, 'utf8') + '</script>\n';
  }
  // minify css content
  if (extension === 'css') {
    var data = fs.readFileSync(file, 'utf8');
    style += new CleanCSS(options).minify(data).styles;
  }
}).end(function() {
  // Minify all javascript files
  var jsMin = uglifyJS.minify(jsTab, {
    outSourceMap: 'dist/brick.map.js'
  });
  // write minified content in new files
  fs.writeFileSync('dist/brick.min.js', jsMin.code, 'utf8');
  fs.writeFileSync('dist/brick.map.js', jsMin.map, 'utf8');
  fs.writeFileSync(options.target, style, 'utf8');
  fs.writeFileSync('dist/templates.min.html', templateFileContent, 'utf8');
});

