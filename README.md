# Canopsis-ui-toolbelt


## Overview

Canopsis-ui-toolbelt is a scaffolding tool provided by Capensis to help developers to create and manage frontend bricks for Canopsis.

It aims to provide an easy and interactive way to :

- Follow the guidelines that have been put in place regarding bricks development and maintainance (lint, bricks' entry points generation)
- Maintain the coherence of informations that have to be provided in different files (version number in manifest files and readme, ...)
- Have a consistent compilation toolchain, suitable and adapted for Canopsis environment (code minification)
- Build documentation (at least a part of it) automatically (API doc and readme)
- Have a clear overview of code quality, and show some generic information about the brick.
- Perform ugrade of all the above features automatically, without burdensome process
- Maintain coherence between bricks folder architecture and file contents

Above all, it make bricks build and maintainance easier.

## Prerequisites

To use ```canopsis-ui-toolbelt```, you must have these tools installed on your system :

- node
- npm
- bower : ```sudo npm install -g bower```
- broccoli-cli : ```sudo npm install -g broccoli-cli```
- testem (gwenaelp's fork): ```sudo npm install -g https://github.com/gwenaelp/testem.git```

## Installation

TODO

## Brick management


Canopsis-ui-toolbelt automatically manage bower and npm manifests, linter configuration, bricks entry points, and every file present in the root folder of the brick, except the brick manifest itself (```manifest.json``` file).

### Manifest

The manifest file is located in the root folder of the brick, into the ```manifest.json``` file. It contains information about the brick.

It is the only file into the root folder which is meant to be edited by the brick developer. Every information is replicated automatically into other files of the root folder if needed. For instance, the ```name``` manifest property is copied into the ```package.json``` and into the ```bower.json``` files.

#### Example

Here is an example of a brick manifest :

```
{
  "name": "brick-querybuilder",
  "description": "Query builder editor",
  "version": "0.0.1",
  "repository": {
    "type": "git",
    "url": "https://git.canopsis.net/canopsis/canopsis-webcore.git"
  },
  "author": "Team Canopsis <maintainers@canopsis.org>",
  "license": "AGPL-3.0",
  "bowerDependencies": {
    "jQuery-QueryBuilder": "https://github.com/mistic100/jQuery-QueryBuilder.git#~2.3.0",
    "jquery-editable-select": "https://git.canopsis.net/gpluchon/jquery-editable-select.git",
    "querybuilder-editablekey": "https://git.canopsis.net/gpluchon/querybuilder-editablekey.git"
  },
  "envMode": "production"
}
```

#### Manifest properties

| Property         | Type    | Description                    |
|------------------|---------|--------------------------------|
| name             |string   | Brick name                     |
| description      |string   | Brick description              |
| version          |string   | Brick version                  |
| repository       |object   | Repository info, in the same format as in the ```package.json``` file |
| author           |string   | Author name                    |
| license          |string   | A valid SPDX license identifier|
| bowerDependencies|object   | Bower dependencies. Same format as the ```dependencies``` key in the ```bower.json``` file |
| envMode          |string   | ```development``` or ```production```      |

## Commands

- ```npm run compile```: regenerate every file that is maintained by Canopsis-ui-toolbelt. This command has to be run after installation, before commiting, when adding or removing a source file, and after an update of the toolbelt.
- ```npm run full-compile```: Same as the compile command, with functionnal and unit test handling and reporting.
- ```npm run doc```: regenerate the documentation
- ```npm run minify```: recompile the source folder into a dist folder
- ```npm run lint```: lint source files

## File generation

TODO

## API Documentation

API documentation is generated into the ```doc/jsdoc``` folder thanks to the jsdoc tool.

### Custom tags

Some custom tags have been set up in the jsdoc environment. They are meant to be used to represent concepts that are specific to Canopsis frontend.

| Tag        | Description                    |
|------------|--------------------------------|
| @widget    | Documents a widget             |
| @component | Documents an Ember Component   |
| @adapter   | Documents an Ember Adapter     |
| @serializer| Documents an Ember Serializer  |

## Code compilation

Code compilation is launched by the ```npm run minify``` command. It takes all the source files present in the ```src``` folder, process them, and generate the content of the ```dist``` folder.

- JS files are concatenated and compressed (uglification process), into a ```brick.min.js``` file
- CSS files are concatenated and compressed as well, into a ```brick.min.css``` file
- HBS files are moved into the folder, into the ```templates``` folder.

## Code Quality and client-side CI

Some data and indicators (PKIs) are computed by the ```compile``` and ```full-compile``` scripts. They are present at the end of the ```README.md``` file.

### Continuous Integration

- Lint results are displayed on this section. The lint rules that are applied are those configured into the ```.eslintrc``` file at the root folder of the brick (which is generated by the ```compile``` script).

- Tests results shows up if the build is done via the "full-compile" script. It is not done with the regular ```compile``` script, to preserve a pleasant build time for developers.

### Code notes

This section gathers special comments in code. Comments that starts with ```//TODO``` and ```//FIXME``` are shown in dedicated tables.

## Pre-commit hook

Canopsis-ui-toolbelt hooks a script up at the git commit hook, to check code quality and ensure guidelines have been respected by the brick developer.

This feature is not mature yet, but is still helps to check automatically the code lint and warn the developer in case of code parts not respecting the lint guidelines
