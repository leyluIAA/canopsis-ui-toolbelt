/**
 * Adds @widget, @serializer, @component and @adapter tags to those DocBlocks that should have it
 */
'use strict';

exports.defineTags = function(dictionary) {
  dictionary.defineTag('widget', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.iswidget = true;

        doclet.name = tag.text;
    }
  });

  dictionary.defineTag('adapter', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.isadapter = true;

        doclet.name = tag.text;
    }
  });

  dictionary.defineTag('renderer', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.isrenderer = true;

        doclet.name = 'renderer-' + tag.text;
    }
  });

  dictionary.defineTag('editor', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.iseditor = true;

        doclet.name = 'editor-' + tag.text;
    }
  });

  dictionary.defineTag('schema', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.isschema = true;

        doclet.name = 'schema-' + tag.text;
    }
  });

  dictionary.defineTag('serializer', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.isserializer = true;

        doclet.name = tag.text;
    }
  });

  dictionary.defineTag('component', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.iscomponent = true;

        doclet.name = tag.text;
    }
  });

  dictionary.defineTag('mixin', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.ismixin = true;

        doclet.name = tag.text;
    }
  });

  dictionary.defineTag('class', { 
    onTagged: function(doclet, tag) {
        doclet.addTag('kind', 'module');
        doclet.isclass = true;

        doclet.name = tag.text;
    }
  });

};
